import { request } from '../service'
export default () => ({
  /**
   * @description 广告列表
   * @param {Object} data 携带的信息
   */
  PM_AD_LIST (data = {}) {
    return request({
      url: '/pm/ad/pageList',
      method: 'get',
      data
    })
  },
  /**
   * @description 新增广告
   * @param {Object} data 携带的信息
   */
  PM_AD_ADD (data = {}) {
    return request({
      url: '/pm/ad/save',
      method: 'post',
      data
    })
  },
  /**
   * @description 修改广告
   * @param {Object} data 携带的信息
   */
  PM_AD_UPDATE (data = {}) {
    return request({
      url: '/pm/ad/update',
      method: 'post',
      data
    })
  },
  /**
   * @description 删除广告
   * @param {Object} data 携带的信息
   */
  PM_AD_DELETE (data = {}) {
    return request({
      url: '/pm/ad/delete',
      method: 'get',
      data
    })
  },
  /**
   * @description 广告详情
   * @param {Object} data 携带的信息
   */
  PM_AD_INFO (data = {}) {
    return request({
      url: '/pm/ad/info',
      method: 'get',
      data
    })
  },
  /**
   * @description 发布广告
   * @param {Object} data 携带的信息
   */
  PM_AD_REPORT (data = {}) {
    return request({
      url: '/pm/ad/report',
      method: 'get',
      data
    })
  }
})
