import { request } from '../service'
export default () => ({
  SYS_ANALYSIS(data = {}) {
    return request({
      url: '/pm/event/analysis',
      method: 'get',
      data
    })
  },
  SYS_EVENT(data = {}) {
    return request({
      url: '/pm/event/list',
      method: 'get',
      data
    })
  },
  SYS_DELEVENT(data = {}) {
    return request({
      url: '/pm/event/delete',
      method: 'get',
      data
    })
  },
  SYS_REPLY(data = {}) {
    return request({
      url: '/pm/event/reply',
      method: 'post',
      data
    })
  }
})
