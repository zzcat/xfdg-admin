import { request } from '../service'
export default () => ({
  /**
   * @description 登录
   * @param {Object} data 携带的信息
   */
  SYS_USER_LOGIN (data = {}) {
    return request({
      url: '/sys/login',
      method: 'post',
      data
    })
  },
  /**
   * @description 用户信息
   * @param {Object} data 携带的信息
   */
  SYS_USER_INFO (data = {}) {
    return request({
      url: '/sys/user/info',
      method: 'get',
      data
    })
  }
})
