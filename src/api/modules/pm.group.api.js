import { request } from '../service'
export default () => ({
  /**
   * @description 分组列表
   * @param {Object} data 携带的信息
   */
  PM_GROUP_LIST (data = {}) {
    return request({
      url: '/pm/group/list',
      method: 'get',
      data
    })
  },
  /**
   * @description 新增分组
   * @param {Object} data 携带的信息
   */
  PM_GROUP_ADD (data = {}) {
    return request({
      url: '/pm/group/save',
      method: 'post',
      data
    })
  },
  /**
   * @description 修改分组
   * @param {Object} data 携带的信息
   */
  PM_GROUP_UPDATE (data = {}) {
    return request({
      url: '/pm/group/update',
      method: 'post',
      data
    })
  },
  /**
   * @description 删除分组
   * @param {Object} data 携带的信息
   */
  PM_GROUP_DELETE (data = {}) {
    return request({
      url: '/pm/group/delete',
      method: 'get',
      data
    })
  },
  /**
   * @description 分组详情
   * @param {Object} data 携带的信息
   */
  PM_GROUP_INFO (data = {}) {
    return request({
      url: '/pm/group/info',
      method: 'get',
      data
    })
  },
  /**
   * @description 分组-选择树
   * @param {Object} data 携带的信息
   */
  PM_GROUP_TREE (data = {}) {
    return request({
      url: '/pm/group/treeList',
      method: 'get',
      data
    })
  }
})
