import { request } from '../service'
export default () => ({
  /**
   * @description 日志列表
   * @param {Object} data 携带的信息
   */
  SYS_LOG_LIST (data = {}) {
    return request({
      url: '/sys/appUser/appLoginLogsPageList',
      method: 'get',
      data
    })
  }
})
