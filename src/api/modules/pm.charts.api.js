import { request } from '../service'

export default () => ({
  // 性别-统计
  PM_COUNT_SEX(data = {}) {
    return request({
      url: 'pm/enterprise/screen/sex/statistics',
      method: 'get',
      data
    })
  },
  // 年龄-统计
  PM_COUNT_AGE(data = {}) {
    return request({
      url: 'pm/enterprise/screen/age/statistics',
      method: 'get',
      data
    })
  },
  // 村群租房-统计
  PM_COUNT_HOUSE(data = {}) {
    return request({
      url: 'pm/enterprise/screen/village/statistics',
      method: 'get',
      data
    })
  },
  // 户籍地-统计-省
  PM_COUNT_PROVINCE(data = {}) {
    return request({
      url: 'pm/enterprise/screen/residence/statistics',
      method: 'get',
      data
    })
  },
  // 户籍地-统计-市
  PM_COUNT_CITY(data = {}) {
    return request({
      url: 'pm/enterprise/screen/residence/bar/statistics',
      method: 'get',
      data
    })
  }
})
