import { request } from '../service'
export default () => ({
  /**
   * @description 人员列表
   * @param {Object} data 携带的信息
   */
  SYS_PERSON_LIST (data = {}) {
    return request({
      url: '/sys/appUser/pageList',
      method: 'post',
      data
    })
  },
  /**
   * @description 人员详细
   * @param {Object} data 携带的信息
   */
  SYS_PERSON_INFO (data = {}) {
    return request({
      url: '/sys/appUser/info',
      method: 'get',
      data
    })
  },
  SYS_TEST (data = {}) {
    return request({
      url: '/6919727312359925',
      method: 'get',
      data
    })
  },
  /**
   * @description 人员审核
   * @param {Object} data 携带的信息
   */
  SYS_PERSON_EXAMINE (data = {}) {
    return request({
      url: '/sys/appUser/examine',
      method: 'post',
      data
    })
  },
  /**
   * @description 人员分组
   * @param {Object} data 携带的信息
   */
  SYS_PERSON_GROUP (data = {}) {
    return request({
      url: '/sys/appUser/group',
      method: 'post',
      data
    })
  },
  /**
   * @description 手动积分调整
   * @param {Object} data 携带的信息
   */
  SYS_PERSON_SCORE (data = {}) {
    return request({
      url: '/sys/appUser/updateCredit',
      method: 'post',
      data
    })
  },
  /**
   * @description 会员备注操作
   * @param {Object} data 携带的信息
   */
  SYS_PERSON_REMARK (data = {}) {
    return request({
      url: '/sys/appUser/inRemark',
      method: 'post',
      data
    })
  },
  /**
   * @description 会员积分明细查询
   * @param {Object} data 携带的信息
   */
  SYS_CREDIT_LIST (data = {}) {
    return request({
      url: '/sys/appUser/creditList',
      method: 'get',
      data
    })
  },
  /**
   * @description 个人历史接单记录
   * @param {Object} data 携带的信息
   */
  SYS_PERSON_HISTORY_ORDER (data = {}) {
    return request({
      url: '/sys/appUser/orderPageList',
      method: 'get',
      data
    })
  },
  /**
   * @description 站内发送消息
   * @param {Object} data 携带的信息
   */
  PM_MESSAGE_SEND (data = {}) {
    return request({
      url: '/pm/message/send',
      method: 'post',
      data
    })
  },
  /**
   * @description 消息分页列表
   * @param {Object} data 携带的信息
   */
  PM_MESSAGE_LIST (data = {}) {
    return request({
      url: '/pm/message/list',
      method: 'get',
      data
    })
  }
})
