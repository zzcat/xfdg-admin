import { request } from '../service'

export default () => ({
  // 企业列表
  DP_ENTERPRISE_LIST(data = {}) {
    return request({
      url: '/pm/enterprise/screen/list',
      method: 'get',
      data
    })
  },
  // 企业信息
  DP_ENTERPRISE_INFO(data = {}) {
    return request({
      url: '/pm/enterprise/screen/info',
      method: 'get',
      data
    })
  }
})
