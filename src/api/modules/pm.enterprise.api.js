import { request } from '../service'

export default () => ({
  /**
   * @description 企业列表
   * @param {Object} data 携带的信息
   */
  PM_ENTERPRISE_LIST(data = {}) {
    return request({
      url: '/pm/enterprise/list',
      method: 'get',
      data
    })
  },
  // 新增企业
  PM_ENTERPRISE_ADD(data = {}) {
    return request({
      url: 'pm/enterprise/add',
      method: 'post',
      data
    })
  },
  // 修改企业信息
  PM_ENTERPRISE_UPDATE(data = {}) {
    return request({
      url: 'pm/enterprise/update',
      method: 'post',
      data
    })
  },
  // 查看企业信息
  PM_ENTERPRISE_INFO(data = {}) {
    return request({
      url: 'pm/enterprise/info',
      method: 'get',
      data
    })
  },
  // 删除企业
  PM_ENTERPRISE_DELETE(data = {}) {
    return request({
      url: 'pm/enterprise/delete',
      method: 'post',
      data
    })
  },
  // 人员信息采集
  PM_ENTERPRISE_PERSON(data = {}) {
    return request({
      url: 'pm/enterprise/person/batchSave',
      method: 'post',
      data
    })
  }
})
