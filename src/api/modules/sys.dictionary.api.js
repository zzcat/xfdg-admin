import { request } from '../service'
export default () => ({
  /**
   * @description 新增字典列表
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_ADD (data = {}) {
    return request({
      url: '/sys/dict/type/save',
      method: 'post',
      data
    })
  },
  /**
   * @description 删除字典
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_DELETE (data = {}) {
    return request({
      url: '/sys/dict/type/delete',
      method: 'post',
      data
    })
  },
  /**
   * @description 修改字典
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_UPDATE (data = {}) {
    return request({
      url: '/sys/dict/type/update',
      method: 'post',
      data
    })
  },
  /**
   * @description 查询字典列表
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_LIST (data = {}) {
    return request({
      url: '/sys/dict/type/pageList',
      method: 'get',
      data
    })
  },
  /**
   * @description 查询字典项列表
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_ITEM_LIST (data = {}) {
    return request({
      url: '/sys/dict/item/pageList',
      method: 'get',
      data
    })
  },
  /**
   * @description 新增字典项
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_ITEM_ADD (data = {}) {
    return request({
      url: '/sys/dict/item/save',
      method: 'post',
      data
    })
  },
  /**
   * @description 修改字典项
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_ITEM_UPDATE (data = {}) {
    return request({
      url: '/sys/dict/item/update',
      method: 'post',
      data
    })
  },
  /**
   * @description 删除字典项
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_ITEM_DELETE (data = {}) {
    return request({
      url: '/sys/dict/item/delete',
      method: 'get',
      data
    })
  },
  /**
   * @description 加载字典下拉框使用
   * @param {Object} data 携带的信息
   */
  SYS_DICTIONARY_LOAD (data = {}) {
    return request({
      url: '/sys/dict/type/load',
      method: 'get',
      data
    })
  }
})
