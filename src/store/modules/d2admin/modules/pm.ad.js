import { Message } from 'element-ui'
import api from '@/api'

export default {
  namespaced: true,
  actions: {
    // 广告列表
    async adList ({ dispatch }, data = {}) {
      const res = await api.PM_AD_LIST(data)
      if (res.code === 0) {
        return res.page
      }
    },
    // 新增广告
    async adAdd ({ dispatch }, data = {}) {
      const res = await api.PM_AD_ADD(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 修改广告
    async adUpdate ({ dispatch }, data = {}) {
      const res = await api.PM_AD_UPDATE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 删除广告
    async adDelete ({ dispatch }, data = {}) {
      const res = await api.PM_AD_DELETE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 广告详情
    async adInfo ({ dispatch }, data = {}) {
      const res = await api.PM_AD_INFO(data)
      if (res.code === 0) {
        return res.data
      }
    },
    // 发布广告
    async adReport ({ dispatch }, data = {}) {
      const res = await api.PM_AD_REPORT(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    }
  }
}
