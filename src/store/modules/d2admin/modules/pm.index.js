import api from '@/api'
import { Message } from 'element-ui'

export default {
  namespaced: true,
  actions: {
    async getAnalysis({ dispatch }, data = {}) {
      const res = await api.SYS_ANALYSIS(data)
      if (res.code === 0) {
        return res.data
      }
    },
    async getEvent({ dispatch }, data = {}) {
      const res = await api.SYS_EVENT(data)
      if (res.code === 0) {
        return res.page
      }
    },
    async deleteEvent({ dispatch }, data = {}) {
      const res = await api.SYS_DELEVENT(data)
      if (res.code === 0) {
        return res.data
      }
    },
    async postReply({ dispatch }, data = {}) {
      const res = await api.SYS_REPLY(data)
      if (res.code === 0) {
        Message.success('回复成功')
        return res
      }
    }
  }
}
