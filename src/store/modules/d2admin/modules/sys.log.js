import api from '@/api'

export default {
  namespaced: true,
  actions: {
    // 日志列表
    async logList ({ dispatch }, data = {}) {
      const res = await api.SYS_LOG_LIST(data)
      if (res.code === 0) {
        return res.page
      }
    }
  }
}
