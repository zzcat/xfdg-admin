import { Message } from 'element-ui'
import api from '@/api'

export default {
  namespaced: true,
  actions: {
    // 分组列表
    async groupList ({ dispatch }, data = {}) {
      const res = await api.PM_GROUP_LIST(data)
      if (res.code === 0) {
        return res.data
      }
    },
    // 新增分组
    async groupAdd ({ dispatch }, data = {}) {
      const res = await api.PM_GROUP_ADD(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 修改分组
    async groupUpdate ({ dispatch }, data = {}) {
      const res = await api.PM_GROUP_UPDATE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 删除分组
    async groupDelete ({ dispatch }, data = {}) {
      const res = await api.PM_GROUP_DELETE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 分组详情
    async groupInfo ({ dispatch }, data = {}) {
      const res = await api.PM_GROUP_INFO(data)
      if (res.code === 0) {
        return res.data
      }
    },
    // 分组选择树
    async groupTree ({ dispatch }, data = {}) {
      const res = await api.PM_GROUP_TREE(data)
      if (res.code === 0) {
        return res.data
      }
    }
  }
}
