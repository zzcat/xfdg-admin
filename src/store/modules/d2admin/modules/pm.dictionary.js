import { Message } from 'element-ui'
import util from '@/libs/util.js'
import api from '@/api'

export default {
  namespaced: true,
  actions: {
    // 查询字典
    async dictList ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_LIST(data)
      if (res.code === 0) {
        return res.page
      }
    },
    // 新增字典
    async dictAdd ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_ADD(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 更新字典
    async dictUpdate ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_UPDATE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 删除字典
    async dictDelete ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_DELETE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 字典项
    // 查询字典项
    async dictItemList ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_ITEM_LIST(data)
      if (res.code === 0) {
        return res.page
      }
    },
    // 新增字典项
    async dictItemAdd ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_ITEM_ADD(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 更新字典项
    async dictItemUpdate ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_ITEM_UPDATE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 删除字典项
    async dictItemDelete ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_ITEM_DELETE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 加载
    async dictLoad ({ dispatch }, data = {}) {
      const res = await api.SYS_DICTIONARY_LOAD(data)
      if (res.code === 0) {
        util.cookies.set('dictionary', res.data)
        return res.data
      }
    }
  }
}
