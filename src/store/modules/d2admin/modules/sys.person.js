import { Message } from 'element-ui'
import api from '@/api'

export default {
  namespaced: true,
  actions: {
    // 人员列表
    async personList ({ dispatch }, data = {}) {
      const res = await api.SYS_PERSON_LIST(data)
      if (res.code === 0) {
        return res.page
      }
    },
    // 人员详情
    async personInfo ({ dispatch }, data = {}) {
      const res = await api.SYS_PERSON_INFO(data)
      if (res.code === 0) {
        return res.data
      }
    },
    // 人员审核
    async personExamine ({ dispatch }, data = {}) {
      const res = await api.SYS_PERSON_EXAMINE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 手动积分调整
    async personScore ({ dispatch }, data = {}) {
      const res = await api.SYS_PERSON_SCORE(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 人员分组
    async personGroup ({ dispatch }, data = {}) {
      const res = await api.SYS_PERSON_GROUP(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 会员备注操作
    async personRemark ({ dispatch }, data = {}) {
      const res = await api.SYS_PERSON_REMARK(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 会员积分明细查询
    async creditList ({ dispatch }, data = {}) {
      const res = await api.SYS_CREDIT_LIST(data)
      if (res.code === 0) {
        return res.data
      }
    },
    // 历史派单
    async personHistoryOrder ({ dispatch }, data = {}) {
      const res = await api.SYS_PERSON_HISTORY_ORDER(data)
      if (res.code === 0) {
        return res.page
      }
    },
    // 站内发送消息
    async sendMessage ({ dispatch }, data = {}) {
      const res = await api.PM_MESSAGE_SEND(data)
      if (res.code === 0) {
        Message({ message: '操作成功！', type: 'success' })
        return res
      }
    },
    // 消息分页列表
    async messageList ({ dispatch }, data = {}) {
      const res = await api.PM_MESSAGE_LIST(data)
      if (res.code === 0) {
        return res.page
      }
    }
  }
}
