const menuData = [
  { title: '首页', icon: 'line-chart', path: '/index' },
  { title: '事件列表', icon: 'laptop', path: '/event' },
  { title: '人员信息', icon: 'user-o', path: '/person' },
  { title: '分组管理', icon: 'address-book-o', path: '/group' },
  { title: '企业管理', icon: 'book', path: '/enterprise' },
  { title: '字典管理', icon: 'code-fork', path: '/dictionary' },
  { title: '新闻管理', icon: 'podcast', path: '/ad' },
  { title: '公告管理', icon: 'bullhorn', path: '/announcement' },
  { title: '日志管理', icon: 'terminal', path: '/logs' }
]
export default menuData
