import layoutHeaderAside from '@/layout/header-aside'

// 由于懒加载页面太多的话会造成webpack热更新太慢，所以开发环境不使用懒加载，只有生产环境使用懒加载
const _import = require('@/libs/util.import.' + process.env.NODE_ENV)

/**
 * 在主框架内显示
 */
const frameIn = [
  {
    path: '/',
    redirect: { name: 'index' },
    component: layoutHeaderAside,
    children: [
      // 首页
      {
        path: 'index',
        name: 'index',
        component: _import('index')
      },
      // 事件
      {
        path: 'event',
        name: 'event',
        meta: {
          title: '事件列表'
        },
        component: _import('event')
      },
      // 人员
      {
        path: 'person',
        name: 'person',
        meta: {
          title: '人员信息'
        },
        component: _import('person')
      },
      // 字典
      {
        path: 'dictionary',
        name: 'dictionary',
        meta: {
          title: '字典管理'
        },
        component: _import('dictionary')
      },
      // 分组
      {
        path: 'group',
        name: 'group',
        meta: {
          title: '分组管理'
        },
        component: _import('group')
      },
      // 分组
      {
        path: 'enterprise',
        name: 'enterprise',
        meta: {
          title: '企业管理'
        },
        component: _import('enterprise')
      },
      // 广告
      {
        path: 'ad',
        name: 'ad',
        meta: {
          title: '新闻管理'
        },
        component: _import('ad')
      },
      // 公告管理
      {
        path: 'announcement',
        name: 'announcement',
        meta: {
          title: '公告管理'
        },
        component: _import('announcement')
      },
      // 日志
      {
        path: 'logs',
        name: 'logs',
        meta: {
          title: '日志管理'
        },
        component: _import('logs')
      },
      // 刷新页面 必须保留
      {
        path: 'refresh',
        name: 'refresh',
        hidden: true,
        component: _import('function/refresh')
      },
      // 页面重定向 必须保留
      {
        path: 'redirect/:route*',
        name: 'redirect',
        hidden: true,
        component: _import('function/redirect')
      }
    ]
  }
]

/**
 * 在主框架之外显示
 */
const frameOut = [
  // 登录
  {
    path: '/login',
    name: 'login',
    component: _import('login')
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: _import('dashboard'),
    redirect: '/dashboard/index',
    children: [
      {
        path: 'index',
        name: 'ManagePage',
        component: _import('dashboard/ManagePage')
      },
      {
        path: 'charts',
        name: 'ChartsPage',
        component: _import('dashboard/ChartsPage')
      }
    ]
  }
]

/**
 * 错误页面
 */
const errorPage = [
  {
    path: '*',
    name: '404',
    component: _import('error/404')
  }
]

// 导出需要显示菜单的
export const frameInRoutes = frameIn

// 重新组织后导出
export default [
  ...frameIn,
  ...frameOut,
  ...errorPage
]
